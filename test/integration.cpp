/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#include "nclq.hh"
#include "DelimTest.hh"
#include "gtest/gtest.h"

extern const char *data_directory;

TEST_F(DelimTest, simpleCSVTest) {
    std::vector<std::string> queries {
        "contains(col1, \"string\")",
        "col5 > 810M",
        "col1 =~ \".*\\s+.*\"",
    };
    std::vector<std::string> outputQ {
        "col1,col4,col2,col3,memory(col5),min(col2,col3)"
    };
    std::vector<nclq::Expression*> expr;
    std::vector<nclq::Expression*> outputE;
    ASSERT_NO_THROW ({
        parser->parseExpression(queries, expr, NULL);
        parser->parseExpression(outputQ, outputE, NULL);
    });

    std::string path = std::string(data_directory) + "/test.csv";
    DelimSource *source = new DelimSource(path, ',');
    nclq::Expression *nullPtr = NULL;
    int counter = 0;
    int foundCount = 0;
    while (source->getNext()) {
        nclq::Data *data = new nclq::Data[symbols->size()];
        for (auto it: expr) {
            nclq::Expression *result = it->evaluateExpression(source, &data);
            ASSERT_NE(nullPtr, result);
            ASSERT_EQ(nclq::EXPR_BOOL, result->getExpressionType());

            nclq::BoolExpression *boolExpr = static_cast<nclq::BoolExpression*>(result);
            if (boolExpr->getValue()) {
                foundCount++;
                for (auto outIt: outputE) {
                    nclq::Expression *outRes = outIt->evaluateExpression(source, &data);
                    ASSERT_NE(nullPtr, outRes);
                    outRes->output(std::cout, source, &data);
                    std::cout << "|";
                    delete outRes;
                }
                std::cout << std::endl;
            }
            delete result;
        }
        delete[] data;
        counter++;
    }
    EXPECT_EQ(4, counter);
    EXPECT_EQ(4, foundCount);

    for (auto it: expr) {
        delete it;
    }
    for (auto it: outputE) {
        delete it;
    }
    delete source;

};
