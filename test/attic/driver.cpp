#include <nclq.hh>
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>
#include "ParseTree.hh"


class DelimSource : private nclq::DataSource {
    char **row;
    int count;
    int size;
    std::string filename;

    public:
    static const std::vector<nclq::VarDescriptor> symbols;

    virtual bool getNext() {
    }

    virtual bool setValue(int idx, nclq::Data *data) {
        if (data == NULL || row == NULL) {
            return false;
        }
        switch (idx) {
            case 0:
                data->setValue(row[0]);
                break;
            case 1:
                data->setValue(atoi(row[1]));
                break;
            case 2:
                data->setValue(atof(row[2]));
                break;
            case 3:
                data->setValue(strcmp(row[3], "true") == 0);
                break;
            default:
                return false;
        }
        return true;
    }
};
const std::vector<nclq::VarDescriptor> DelimSource::symbols = {
    {"col1", nclq::T_STRING,  nclq::T_STRING},
    {"col2", nclq::T_INTEGER, nclq::T_INTEGER},
    {"col3", nclq::T_DOUBLE,  nclq::T_DOUBLE},
    {"col4", nclq::T_BOOL,    nclq::T_BOOL},
};

int main(int argc, char **argv) {
    /* test driver */
    nclq::nclqParser *parser = new nclq::nclqParser();
    parser->setVerbose(true);
    parser->symbols = new nclq::SymbolTable(DelimSource::symbols);

    std::vector<std::string> query_strings{
        "col2 * 5 == 100",
        "(col2 < 1 || col2 > 5) && contains(col1, \"test\")",
        "col4 && col2 < 0.5",
    };
    std::vector<nclq::Expression *>  query_expressions;

    parser->parseExpression(query_strings, query_expressions, NULL);
    for (auto it: query_expressions) {
        delete it;
    }

    delete parser;

}
