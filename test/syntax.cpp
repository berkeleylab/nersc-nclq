/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>

#include "nclq.hh"
#include "DelimTest.hh"
#include "gtest/gtest.h"

void checkQueryExpression(const std::string& exprString, const nclq::Expression* expr) {
    if (expr == NULL) {
        throw std::invalid_argument("Null expression!");
    }
    if (expr->getBaseType() != nclq::T_BOOL) {
        throw std::invalid_argument("Query expression must evaluate to a boolean!");
    }
    if (expr->hasType(nclq::EXPR_ASSIGN)) {
        throw std::invalid_argument("Assignments not allowed in query expressions.");
    }
}

void checkDeclarationExpression(const std::string& exprString, const nclq::Expression* expr) {
    if (expr == NULL || expr->getExpressionType() != nclq::EXPR_ASSIGN) {
        throw std::invalid_argument("Declaration expressions must be assignments.");
    }
}

TEST_F(DelimTest, orderop_mathematical) {
    std::vector<int> answers {6,6,-4,4,7,-9,3};
    std::vector<std::string> exp_strings {
        "5 + 4 - 3",
        "4 - 3 + 5",
        "4 - (3 + 5)",
        "5 + -1",
        "2^3 - 1",
        "-2^3 - 1",
        "4 *     7/9",
    };
    std::vector<nclq::Expression*> exp;
    ASSERT_NO_THROW(
        parser->parseExpression(exp_strings, exp, NULL);
    );
    ASSERT_EQ(answers.size(), exp.size());

    int idx = 0;
    for (auto it: exp) {
        nclq::Expression *expr = it;
        nclq::Expression *result = expr->evaluateExpression(NULL, NULL);
        ASSERT_EQ(nclq::EXPR_INTEGER, result->getExpressionType());
        nclq::NumericalExpression *numexpr = static_cast<nclq::NumericalExpression*>(result);
        EXPECT_EQ(answers[idx++], numexpr->getIntegerValue());

        delete it;
        delete result;
    }
}

TEST_F(DelimTest, orderop_relative) {
    std::vector<bool> answers {true, false, true, false, true, true, true, true, true, true, true, true, true, true, true, false};
    std::vector<std::string> exp_strings {
        "1+2 == 5-2",
        "1+2 <  5-2",
        "true",
        "false",
        "\"asdf\" == \"asdf\"",
        "\"asdf\" != \"qwerty\"",
        "\"asdf\" < \"qwerty\"",
        "false == !true",
        "true == !!true",
        "-1 == -(10/10)",
        "false < true",
        "10 < 5 || 5 < 10",
        "10 < 5 == (\"red\" == \"blue\")",
        "1.01 == 5.05/5",
        "1.01 < 2 * 5",
        "true && false || false && true && true || false",
    };
    std::vector<nclq::Expression*> exp;
    ASSERT_NO_THROW(
        parser->parseExpression(exp_strings, exp, NULL);
    );
    ASSERT_EQ(answers.size(), exp.size());

    int idx = 0;
    const nclq::Expression *nullExpr = NULL;
    for (auto it: exp) {
        nclq::Expression *result = it->evaluateExpression(NULL, NULL);
        ASSERT_NE(nullExpr, result);
        ASSERT_EQ(nclq::EXPR_BOOL, result->getExpressionType());
        nclq::BoolExpression *boolexpr = static_cast<nclq::BoolExpression*>(result);
        EXPECT_EQ(answers[idx++], boolexpr->getValue());

        delete it;
        delete result;
    }
}

TEST_F(DelimTest, declaration) {
    symbols->setModifiable(true);
    std::vector<std::string> decl_strings {
        "a = col2 * 5",
        "b = col2 > 1",
        "c = contains(col1, \"test\")",
    };
    std::vector<nclq::Expression*> decl_exp;
    ASSERT_NO_THROW(
        parser->parseExpression(decl_strings, decl_exp, checkDeclarationExpression);
    );
    EXPECT_EQ(3, decl_exp.size());
    EXPECT_NE(-1, symbols->find("a"));
    EXPECT_NE(-1, symbols->find("b"));
    EXPECT_NE(-1, symbols->find("c"));
    EXPECT_EQ(-1, symbols->find("fake"));

    /* can use declared fields? */
    decl_strings.clear();
    decl_strings.push_back("d = a + b");
    ASSERT_NO_THROW(
        parser->parseExpression(decl_strings, decl_exp, checkDeclarationExpression);
    );
    EXPECT_EQ(4, decl_exp.size());
    for (auto it: decl_exp) {
        delete it;
    }
}

TEST_F(DelimTest, no_assign) {
    symbols->setModifiable(true);
    std::vector<std::string> query_strings{
        "col2 * 5 == 100",
        "(col2 < 1 || col2 > 5) && contains(col1, \"test\")",
        "col4 && col2 < 0.5",
    };
    std::vector<nclq::Expression *>  query_expressions;
    ASSERT_NO_THROW(
        parser->parseExpression(query_strings, query_expressions, checkQueryExpression);
    );

    EXPECT_EQ(3, query_expressions.size());

    query_strings.clear();
    query_strings.push_back("col2 == 5*8");
    ASSERT_NO_THROW(
        parser->parseExpression(query_strings, query_expressions, checkQueryExpression);
    );
    EXPECT_EQ(4, query_expressions.size());

    query_strings.clear();
    query_strings.push_back("newField = col2*3");

    EXPECT_THROW(
        parser->parseExpression(query_strings, query_expressions, checkQueryExpression),
        std::invalid_argument
    );

    query_strings.clear();
    query_strings.push_back("col2 == 5*8, col2 != 4, newField = col2*3");
    EXPECT_THROW(
        parser->parseExpression(query_strings, query_expressions, checkQueryExpression),
        std::invalid_argument
    );

    for (auto it: query_expressions) {
        delete it;
    }
}

TEST_F(DelimTest, ExprList_test) {
    std::vector<std::string> q {
        "col1,col2,col3"
    };
    std::vector<nclq::Expression*> query_expressions;
    ASSERT_NO_THROW(
        parser->parseExpression(q, query_expressions, NULL);
    );
    EXPECT_EQ(3, query_expressions.size());
    for (auto it: query_expressions) {
        delete it;
    }
}
