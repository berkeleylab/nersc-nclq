/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include "nclq.hh"
#include "gtest/gtest.h"
#include "DelimTest.hh"

DelimSource::DelimSource(const std::string& _filename, char _delim):
    filename(_filename), delim(_delim)
{
    fp = NULL;
    sptr = ptr = eptr = NULL;
    inQuote = false;
    colIdx = 0;
    row = NULL;
}

DelimSource::~DelimSource() {
    if (fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    if (row != NULL) {
        for (int i = 0; i < size(); ++i) {
            if (row[i] != NULL) {
                free(row[i]);
                row[i] = NULL;
            }
        }
        delete[] row;
        row = NULL;
    }
}

bool DelimSource::openFile() {
    if (fp != NULL) return false;
    fp = fopen(filename.c_str(), "r");
    return fp != NULL;
}

ssize_t DelimSource::fillBuffer() {
    if (fp == NULL) return false;
    if (sptr != NULL) {
        bcopy(buffer, sptr, sizeof(char)*(ptr - sptr));
        ptr = buffer + (ptr -sptr);
        sptr = buffer;
    } else {
        sptr = buffer;
        ptr = buffer;
    }
    ssize_t readBytes = fread(ptr, sizeof(char), buffer_size - (ptr - sptr), fp);
    eptr = ptr + readBytes;
    return readBytes;
}

bool DelimSource::getNext() {
    if (fp == NULL) openFile();
    ssize_t rbytes = 0;
    if (row == NULL) {
        row = new char*[size()];
        memset(row, 0, sizeof(char*) * size());
    } else {
        for (int i = 0; i < size(); i++) {
            if (row[i] != NULL) {
                free(row[i]);
                row[i] = NULL;
            }
        }
    }
    colIdx = 0;
    for ( ; ; ptr++) {
        if (sptr == NULL || ptr == eptr) {
            rbytes = fillBuffer();
            if (rbytes == 0) break;
        }
        if (*ptr == '\n' || *ptr == 0) {
            /* we're done -- no matter what */
            inQuote = false;
            *ptr = 0;
            if (colIdx < size()) {
                row[colIdx++] = strdup(sptr);
            }
            sptr = ptr + 1;
            ptr = sptr;
            break;
        }
        if (inQuote) {
            if (*ptr == '\"') inQuote = false;
            continue;
        }
        if (*ptr == delim) {
            *ptr = 0;
            if (colIdx < size()) {
                row[colIdx++] = strdup(sptr);
            }
            sptr = ptr + 1;
        }
    }
    return colIdx == size();
}

const std::vector<nclq::VarDescriptor> DelimSource::symbols = {
    {"col1", "First Column", nclq::T_STRING,  nclq::T_STRING},
    {"col2", "Second Column", nclq::T_INTEGER, nclq::T_INTEGER},
    {"col3", "Third Column", nclq::T_DOUBLE,  nclq::T_DOUBLE},
    {"col4", "Fourth Column", nclq::T_BOOL,    nclq::T_BOOL},
    {"col5", "Fifth Column", nclq::T_MEMORY,  nclq::T_DOUBLE},
};

void DelimSource::outputValue(std::ostream& out, int idx) {
    out << row[idx];
}

int DelimSource::size() {
    return DelimSource::symbols.size();
}

bool DelimSource::setValue(int idx, nclq::Data *data, nclq::SymbolType outputType) {
    if (data == NULL || row == NULL) {
        return false;
    }
    if (row[idx] == NULL) return false;
    switch (idx) {
        case 0:
            data->setValue(row[0], outputType);
            break;
        case 1:
            if (strlen(row[idx]) > 0) {
                data->setValue((intType)atoi(row[1]), outputType);
            }
            break;
        case 2:
            if (strlen(row[idx]) > 0) {
                data->setValue(atof(row[2]), outputType);
            }
            break;
        case 3:
            data->setValue(strcmp(row[3], "true") == 0, outputType);
            break;
        case 4:
            data->setValue(nclq::util::convertMemory(row[4]), outputType);
            break;
        default:
            return false;
    }
    return true;
}
