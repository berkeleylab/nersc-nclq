/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#include <gtest/gtest.h>

const char *data_directory = NULL;

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);

    data_directory = ".";
    if (argc > 1) {
        data_directory = argv[1];
    }
    return RUN_ALL_TESTS();
}
