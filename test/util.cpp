/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>

#include "nclq.hh"
#include "gtest/gtest.h"


TEST(UtilTest, convertTime) {
    std::vector<std::string> time_strings {
        "4000",
        "0:0:0",
        "0:05:10",
        "01:01:01",
        "12:00:00",
        "23:59:59",
        "5:10",
    };
    std::vector<int> answers {4000, 0, 310, 3661, 43200, 86399, 310};

    int idx = 0;
    for (auto &it: time_strings) {
        const char *str = it.c_str();
        EXPECT_EQ(answers[idx++], nclq::util::convertTime(str));
    }
}

TEST(UtilTest, convertMemory) {
    std::vector<std::string> mem_strings {
        "1000",
        "1M",
        "1MB",
        "1m",
        "1gb",
    };
    std::vector<double> answers { 1000, 1024*1024, 1024*1024, 1024*1024, 1024*1024*1024 };
    int idx = 0;
    for (auto &it: mem_strings) {
        const char *str = it.c_str();
        EXPECT_EQ(answers[idx++], nclq::util::convertMemory(str));
    }
}
