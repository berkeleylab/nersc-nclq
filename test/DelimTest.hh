#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include "nclq.hh"
#include "gtest/gtest.h"


class DelimSource : public nclq::DataSource {
    char **row;
    std::string filename;
    char delim;

    FILE *fp;
    static const int buffer_size = 131072;
    char buffer[buffer_size];
    char *sptr;
    char *ptr;
    char *eptr;

    bool inQuote;
    int colIdx;

    bool openFile();
    ssize_t fillBuffer();

    public:
    static const std::vector<nclq::VarDescriptor> symbols;

    DelimSource(const std::string& filename, char delim);
    ~DelimSource();

    virtual bool getNext();
    virtual bool setValue(int idx, nclq::Data *data, nclq::SymbolType outputType);
    virtual int size();
    virtual void outputValue(std::ostream& out, int idx);
};


class DelimTest : public ::testing::Test {
    protected:
    virtual void SetUp() {
        symbols = new nclq::SymbolTable(DelimSource::symbols);
        symbols->setModifiable(true);
        symbols->mapSymbol("col1_again", 0);
        symbols->mapSymbol("col1_repr", 0);
        symbols->setModifiable(false);
        parser = new nclq::nclqParser(symbols);
    }

    virtual void TearDown() {
        cout << "SYMBOL TABLE" << endl;
        symbols->printSymbolTable();
        delete symbols;
        delete parser;
    }

    nclq::nclqParser *parser;
    nclq::SymbolTable *symbols;
};
