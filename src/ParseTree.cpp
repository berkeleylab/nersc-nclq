/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#ifndef __NCLQ_PARSETREE
#define __NCLQ_PARSETREE

#include <string>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <limits>

#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
#include "nclq.hh"
#include "Function.hh"

using namespace std;

namespace nclq {

static const char* idMap[] = { "UNKNOWN", "NAME", "FLOAT", "INTEGER", "STRING",
    "PLUS", "MINUS", "TIMES", "DIVIDE", "BOOL", "MODULUS", "EXPONENT", "EQUAL",
    "NEQUAL", "LESS", "GREATER", "LESSEQUAL", "GREATEREQUAL", "AND", "OR",
    "NOT", "GROUP", "ASSIGN", "VARIABLE", "FUNCTION", "EXPRESSIONLIST",
};

static const int fxnCount=9;
static const SymbolType fxnType[] = {
    T_STRING, T_STRING, T_STRING, T_DOUBLE, T_DOUBLE, T_DOUBLE, T_INTEGER,
    T_DOUBLE, T_BOOL
};
static const char* fxnMap[] = {
    "date", "datetime", "memory", "min", "max", "sum", "count",
    "mean", "contains"
};

/**********
  ExpressionException    IMPLEMENTATION
***********/
ExpressionException::ExpressionException(const char* t_token,
        ExpressionType t_type, const char* sup):
    runtime_error(sup), token(t_token), type(t_type)
{
}
ExpressionException::ExpressionException(const char* t_token,
        ExpressionType t_type, string& sup):
    runtime_error(sup), token(t_token), type(t_type)
{
}

const char *ExpressionException::getToken() {
    return token;
}

/***********
  Expression ABSTRACT BASE CLASS IMPLEMENTATION
***********/
Expression::Expression() {
    expressionType = EXPR_UNKNOWN;
    expressionTypeIdentifier = idMap[EXPR_UNKNOWN];
}
Expression::~Expression() {
}
void Expression::printExpressionTree(int level) {
}
bool Expression::hasType(ExpressionType type) const {
    return type == expressionType;
}
Expression* Expression::evaluateExpression(DataSource *source, Data **data) {
    return NULL;
}
const ExpressionType Expression::getExpressionType() const {
    return expressionType;
}
const SymbolType Expression::getBaseType() const {
    return baseType;
}
ostream& Expression::output(ostream& out, DataSource *source,
        Data** data)
{
    out << "BASE CLASS UNDEF!"; return out;
}

/***********
  BoolExpression IMPLEMENTATION
***********/
BoolExpression::BoolExpression(bool t_value): value(t_value) {
    this->expressionType = EXPR_BOOL;
    this->baseType = T_BOOL;
    this->expressionTypeIdentifier = idMap[EXPR_BOOL];
}
void BoolExpression::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    cout << "Bln: " << value << endl;
}
const bool BoolExpression::getValue() const {
    return value;
}
Expression* BoolExpression::evaluateExpression(DataSource *source,
        Data** data)
{
    BoolExpression* retValue = new BoolExpression(value);
    return retValue;
}
BoolExpression* BoolExpression::compare(int type, BoolExpression* b) {
    switch (type) {
        case EXPR_EQUAL:
            this->value = this->value == b->value;
            break;
        case EXPR_NEQUAL:
            this->value = this->value != b->value;
            break;
        case EXPR_LESS:
            this->value = this->value < b->value;
            break;
        case EXPR_GREATER:
            this->value = this->value > b->value;
            break;
        case EXPR_LESSEQUAL:
            this->value = this->value <= b->value;
            break;
        case EXPR_GREATEREQUAL:
            this->value = this->value >= b->value;
            break;
        case EXPR_AND:
            this->value = this->value && b->value;
            break;
        case EXPR_OR:
            this->value = this->value || b->value;
            break;
    }
    delete b;
    return this;
}
ostream& BoolExpression::output(ostream& out, DataSource *source,
        Data** data)
{
    out << (value ? "True" : "False");
    return out;
}

/***********
  NumericalExpression IMPLEMENTATION
***********/
const intType NumericalExpression::getIntegerValue() const {
    bool isFloat = expressionType == EXPR_FLOAT;
    if (isFloat) return (intType)d_value;
    return i_value;
}
const double NumericalExpression::getDoubleValue() const {
    bool isFloat = expressionType == EXPR_FLOAT;
    if (!isFloat) return (double)i_value;
    return d_value;
}
NumericalExpression* NumericalExpression::sum(NumericalExpression* ptr) {
    bool otherFloat = ptr->expressionType == EXPR_FLOAT;
    bool selfFloat = this->expressionType == EXPR_FLOAT;
    if (!otherFloat && !selfFloat) {
        // integer math & can return self
        this->i_value += ptr->i_value;
        delete ptr;
        return this;
    }
    if (selfFloat) {
        // this is a float, sum and return self
        this->d_value += otherFloat ? ptr->d_value : ptr->i_value;
        delete ptr;
        return this;
    }
    this->d_value = this->i_value + ptr->d_value;
    expressionType = EXPR_FLOAT;
    baseType = T_DOUBLE;
    expressionTypeIdentifier = idMap[EXPR_FLOAT];
    delete ptr;
    return this;
}
NumericalExpression* NumericalExpression::multiply(NumericalExpression* ptr) {
    bool otherFloat = ptr->expressionType == EXPR_FLOAT;
    bool selfFloat = this->expressionType == EXPR_FLOAT;
    if (!otherFloat && !selfFloat) {
        // integer math & can return self
        this->i_value *= ptr->i_value;
        delete ptr;
        return this;
    }
    if (selfFloat) {
        // this is a float, sum and return self
        this->d_value *= otherFloat ? ptr->d_value : ptr->i_value;
        delete ptr;
        return this;
    }
    this->d_value = this->i_value * ptr->d_value;
    expressionType = EXPR_FLOAT;
    baseType = T_DOUBLE;
    expressionTypeIdentifier = idMap[EXPR_FLOAT];
    delete ptr;
    return this;
}
NumericalExpression* NumericalExpression::divide(NumericalExpression* ptr) {
    bool otherFloat = ptr->expressionType == EXPR_FLOAT;
    bool selfFloat = this->expressionType == EXPR_FLOAT;
    if (!otherFloat && !selfFloat) {
        // integer math & can return self
        this->i_value /= ptr->i_value;
        delete ptr;
        return this;
    }
    if (selfFloat) {
        // this is a float, sum and return self
        this->d_value /= otherFloat ? ptr->d_value : ptr->i_value;
        delete ptr;
        return this;
    }
    this->d_value = this->i_value / ptr->d_value;
    expressionType = EXPR_FLOAT;
    baseType = T_DOUBLE;
    expressionTypeIdentifier = idMap[EXPR_FLOAT];
    delete ptr;
    return this;
}
NumericalExpression* NumericalExpression::subtract(NumericalExpression* ptr) {
    bool otherFloat = ptr->expressionType == EXPR_FLOAT;
    bool selfFloat = this->expressionType == EXPR_FLOAT;
    if (!otherFloat && !selfFloat) {
        // integer math & can return self
        this->i_value -= ptr->i_value;
        delete ptr;
        return this;
    }
    if (selfFloat) {
        // this is a float, sum and return self
        this->d_value -= otherFloat ? ptr->d_value : ptr->i_value;
        delete ptr;
        return this;
    }
    this->d_value = this->i_value - ptr->i_value;
    expressionType = EXPR_FLOAT;
    baseType = T_DOUBLE;
    expressionTypeIdentifier = idMap[EXPR_FLOAT];
    delete ptr;
    return this;
}
NumericalExpression* NumericalExpression::modulus(NumericalExpression* ptr) {
    bool otherFloat = ptr->expressionType == EXPR_FLOAT;
    bool selfFloat = this->expressionType == EXPR_FLOAT;
    if (!otherFloat && !selfFloat) {
        // integer math & can return self
        this->i_value %= ptr->i_value;
        delete ptr;
        return this;
    }

    this->i_value = (selfFloat ? (int)this->d_value : this->i_value) %
                    (otherFloat ? (int)ptr->d_value : ptr->i_value);
    expressionType = EXPR_INTEGER;
    baseType = T_INTEGER;
    expressionTypeIdentifier = idMap[EXPR_INTEGER];
    delete ptr;
    return this;
}
NumericalExpression* NumericalExpression::exponentiate(NumericalExpression* ptr) {
    bool otherFloat = ptr->expressionType == EXPR_FLOAT;
    bool selfFloat = this->expressionType == EXPR_FLOAT;
    if (!otherFloat && !selfFloat) {
        // integer math & can return self
        this->i_value = pow(this->i_value, ptr->i_value);
        delete ptr;
        return this;
    }
    if (selfFloat) {
        // this is a float, sum and return self
        this->d_value = pow(this->d_value, otherFloat ? ptr->d_value : ptr->i_value);
        delete ptr;
        return this;
    }
    this->d_value = pow(this->i_value, ptr->d_value);
    expressionType = EXPR_FLOAT;
    baseType = T_DOUBLE;
    expressionTypeIdentifier = idMap[EXPR_FLOAT];
    delete ptr;
    return this;
}
NumericalExpression::NumericalExpression(intType t_value): i_value(t_value) {
    this->expressionType = EXPR_INTEGER;
    this->baseType = T_INTEGER;
    this->expressionTypeIdentifier = idMap[EXPR_INTEGER];
}
NumericalExpression::NumericalExpression(double t_value): d_value(t_value) {
    this->expressionType = EXPR_FLOAT;
    this->baseType = T_DOUBLE;
    this->expressionTypeIdentifier = idMap[EXPR_FLOAT];
}
void NumericalExpression::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    if (this->baseType == T_INTEGER) {
        cout << "Int: " << i_value << endl;
    } else {
        cout << "Dbl: " << d_value << endl;
    }
}
Expression* NumericalExpression::evaluateExpression(DataSource *source,
        Data** data)
{
    if (this->baseType == T_INTEGER) {
        return new NumericalExpression(i_value);
    }
    return new NumericalExpression(d_value);
}

BoolExpression* NumericalExpression::compare(int type, NumericalExpression* b) {
    BoolExpression* retValue = new BoolExpression(false);
    NumericalExpression* a = this; // laziness, don't want to type right now
    bool aFloat = a->baseType == T_DOUBLE;
    bool bFloat = b->baseType == T_DOUBLE;

    switch (type) {
        case EXPR_EQUAL:
            if (aFloat && bFloat) retValue->value = a->d_value == b->d_value;
            else if (aFloat && !bFloat) retValue->value = a->d_value == b->i_value;
            else if (!aFloat && bFloat) retValue->value = a->i_value == b->d_value;
            else retValue->value = a->i_value == b->i_value;
            break;
        case EXPR_NEQUAL:
            if (aFloat && bFloat) retValue->value = a->d_value != b->d_value;
            else if (aFloat && !bFloat) retValue->value = a->d_value != b->i_value;
            else if (!aFloat && bFloat) retValue->value = a->i_value != b->d_value;
            else retValue->value = a->i_value != b->i_value;
            break;
        case EXPR_LESS:
            if (aFloat && bFloat) retValue->value = a->d_value < b->d_value;
            else if (aFloat && !bFloat) retValue->value = a->d_value < b->i_value;
            else if (!aFloat && bFloat) retValue->value = a->i_value < b->d_value;
            else retValue->value = a->i_value < b->i_value;
            break;
        case EXPR_GREATER:
            if (aFloat && bFloat) retValue->value = a->d_value > b->d_value;
            else if (aFloat && !bFloat) retValue->value = a->d_value > b->i_value;
            else if (!aFloat && bFloat) retValue->value = a->i_value > b->d_value;
            else retValue->value = a->i_value > b->i_value;
            break;
        case EXPR_LESSEQUAL:
            if (aFloat && bFloat) retValue->value = a->d_value <= b->d_value;
            else if (aFloat && !bFloat) retValue->value = a->d_value <= b->i_value;
            else if (!aFloat && bFloat) retValue->value = a->i_value <= b->d_value;
            else retValue->value = a->i_value <= b->i_value;
            break;
        case EXPR_GREATEREQUAL:
            if (aFloat && bFloat) retValue->value = a->d_value >= b->d_value;
            else if (aFloat && !bFloat) retValue->value = a->d_value >= b->i_value;
            else if (!aFloat && bFloat) retValue->value = a->i_value >= b->d_value;
            else retValue->value = a->i_value >= b->i_value;
            break;
    }
    delete b;
    return retValue;
}
ostream& NumericalExpression::output(ostream& out, DataSource *source,
        Data** data)
{
    if (this->baseType == T_DOUBLE) out << this->d_value;
    else out << this->i_value;
    return out;
}

/***********
  StringExpression IMPLEMENTATION
***********/
StringExpression::StringExpression(const char* t_value, bool t_alloc):
    value(t_value), owner(t_alloc)
{
    this->expressionType = EXPR_STRING;
    this->baseType = T_STRING;
    this->expressionTypeIdentifier = idMap[EXPR_STRING];
}
StringExpression::StringExpression(const string& t_value, bool t_alloc):
    value(t_value.c_str()), owner(t_alloc)
{
    this->expressionType = EXPR_STRING;
    this->baseType = T_STRING;
    this->expressionTypeIdentifier = idMap[EXPR_STRING];
}
StringExpression::~StringExpression() {
}
void StringExpression::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    cout << "Str: " << value << endl;
}
Expression* StringExpression::evaluateExpression(DataSource *source,
        Data** data)
{
    StringExpression* retValue = new StringExpression(value);
    return retValue;
}
BoolExpression* StringExpression::compare(int type, StringExpression* b) {
    BoolExpression* retValue = new BoolExpression(false);
    int cmpVal = strcmp(this->value.c_str(), b->value.c_str());

    switch (type) {
        case EXPR_EQUAL:
            retValue->value = cmpVal == 0;
            break;
        case EXPR_NEQUAL:
            retValue->value = cmpVal != 0;
            break;
        case EXPR_LESS:
            retValue->value = cmpVal < 0;
            break;
        case EXPR_GREATER:
            retValue->value = cmpVal > 0;
            break;
        case EXPR_LESSEQUAL:
            retValue->value = cmpVal <= 0;
            break;
        case EXPR_GREATEREQUAL:
            retValue->value = cmpVal >= 0;
            break;
    }
    delete b;
    return retValue;
}
const string& StringExpression::getValue() const {
    return value;
}
ostream& StringExpression::output(ostream& out, DataSource *source,
        Data** data)
{
    out << this->value;
    return out;
}

/***********
  NameExpression IMPLEMENTATION
***********/
NameExpression::NameExpression(const char* t_value, SymbolTable* _symbols):
    varName(t_value), symbols(_symbols)
{
    this->expressionType = EXPR_NAME;
    this->expressionTypeIdentifier = idMap[EXPR_NAME];

    /* check to see if this identifier is already in the symbol table
       if it is, cache the index into the qacct arrays and the type
       if it isn't, add the new symbol to the table and mark the type
         as unknown (will be worked out on the first execution of the
         parse tree)
    */

    ssize_t idx = symbols->find(varName);
    if (idx >= 0) {
        varIdx = idx;
        varType = (*symbols)[varIdx].type;
        varOutputType = (*symbols)[varIdx].outputType;
    } else if (symbols->modifiable()) {
        /* check to see if a fxn name */
        VarDescriptor newVar(varName.c_str(), "", T_UNDEF, T_UNDEF);
        varIdx = symbols->addSymbol(varName, newVar);
        varType = T_UNDEF;
        varOutputType = T_UNDEF;
    } else {
        ExpressionException e(varName.c_str(), EXPR_NAME,
                "Can't define new symbols here.");
        throw e;
    }
    this->baseType = varType;
}

NameExpression::NameExpression(const string& t_varName, int t_varIdx,
        SymbolType t_varType, SymbolTable* _symbols):
    varName(t_varName), varIdx(t_varIdx), varType(t_varType), symbols(_symbols)
{
    this->expressionType = EXPR_NAME;
    this->expressionTypeIdentifier = idMap[EXPR_NAME];
    this->baseType = t_varType;
    this->varOutputType = t_varType;
}

void NameExpression::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    cout << "Var: " << varName << "," << varIdx << "," << (int)varType << endl;
}
Expression* NameExpression::evaluateExpression(DataSource *source, Data** data) {

    if (!(*data)[varIdx].isSet()) {
        source->setValue(varIdx, &((*data)[varIdx]), varOutputType);
    }
    if ((*data)[varIdx].isSet()) {
        unsigned char type = (*data)[varIdx].getType();
        switch (type) {
            case T_INTEGER:
                return new NumericalExpression((*data)[varIdx].getIntValue());
                break;
            case T_MEMORY:
            case T_DOUBLE:
                return new NumericalExpression((*data)[varIdx].getDoubleValue());
            case T_STRING:
                return new StringExpression((*data)[varIdx].getStringValue());
                break;
            case T_BOOL:
                return new BoolExpression((*data)[varIdx].getBoolValue());
                break;
            default: {
                ostringstream oss;
                oss << "Error: no variable type set for " << varIdx << ",";
                oss << (*symbols)[varIdx].name;
                ExpressionException e("NameExpression", expressionType, oss.str().c_str());
                throw &e;
            }
        }
    }
    /* this must be a variable we haven't fully typed yet */
    return new NameExpression(varName, varIdx, varType, symbols);
}
ostream& NameExpression::output(ostream& out, DataSource *source, Data **data) {
    if (!(*data)[varIdx].isSet()) {
        source->setValue(varIdx, &((*data)[varIdx]), varOutputType);
    }
    if ((*data)[varIdx].isSet()) {
        out << (*data)[varIdx];
    } else if (varIdx < source->size()) {
        source->outputValue(out, varIdx);
    } else {
        out << "ERROR";
    }
    return out;
}

void ExpressionList::init() {
    expressionType = EXPR_EXPRESSIONLIST;
    expressionTypeIdentifier = idMap[expressionType];
    baseType = T_UNDEF;
}
ExpressionList::ExpressionList() {
    init();
}
ExpressionList::ExpressionList(Expression* expr1, Expression* expr2) {
    init();
    expressions.push_back(expr1);
    expressions.push_back(expr2);
}
ExpressionList::~ExpressionList() {
    for (auto it: expressions) {
        delete it;
    }
}
void ExpressionList::addExpression(Expression* expr1) {
    expressions.push_back(expr1);
}
void ExpressionList::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    cout << "ExpList: " << endl;
    for (auto iter: expressions) {
        iter->printExpressionTree(level+1);
    }
}
bool ExpressionList::hasType(ExpressionType type) const {
    bool retValue = expressionType == type;
    for (auto iter: expressions) {
        retValue |= iter->hasType(type);
    }
    return retValue;
}

Expression* ExpressionList::evaluateExpression(DataSource *source, Data** data) {
    ExpressionList* ret = new ExpressionList();
    for (auto iter: expressions) {
        ret->expressions.push_back(iter->evaluateExpression(source,
                    data));
    }
    return ret;
}
const vector<Expression*>& ExpressionList::getExpressions() const {
    return expressions;
}
ostream& ExpressionList::output(ostream& out, DataSource *source, Data** data) {
    out << "UNDEF";
    return out;
}

FxnExpression::FxnExpression(const char* t_name, Expression* t_expr, SymbolTable* _symbols):
    fxnName(t_name), expr(t_expr), symbols(_symbols)
{
    this->expressionType = EXPR_FUNCTION;
    this->expressionTypeIdentifier = idMap[expressionType];
    this->baseType = T_UNDEF; // this may need to be changed to reflect the target fxn (if type can be determined statically)
    this->fxnIdx = -1;
    this->functionPtr = NULL;

    for (int i = 0; i < fxnCount; i++) {
        if (fxnName == fxnMap[i]) {
            fxnIdx = i;
            this->baseType = fxnType[i];
            
            break;
        }
    }
    switch (fxnIdx) {
        case -1:
            functionPtr = NULL;
            break;
        case 0:
            functionPtr = new DateFunction();
            break;
        case 1:
            functionPtr = new DateTimeFunction();
            break;
        case 2:
            functionPtr = new MemoryFunction();
            break;
        case 3:
            functionPtr = new MinFunction();
            break;
        case 4:
            functionPtr = new MaxFunction();
            break;
        case 5:
            functionPtr = new SumFunction();
            break;
        case 6:
            functionPtr = new CountFunction();
            break;
        case 7:
            functionPtr = new MeanFunction();
            break;
        case 8:
            functionPtr = new ContainsFunction();
            break;
    }
}
FxnExpression::~FxnExpression() {
    if (functionPtr != NULL) delete functionPtr;
    if (expr != NULL) delete expr;
}
void FxnExpression::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    cout << "Fxn: " << fxnName << "," << endl;
    expr->printExpressionTree(level+1);
}
bool FxnExpression::hasType(ExpressionType type) const {
    return expressionType == type || expr->hasType(type);
}
Expression* FxnExpression::evaluateExpression(DataSource *source, Data** data) {
    Expression* operands = expr->evaluateExpression(source, data);
    Expression* retExpression = NULL;
    ExpressionList* opList = NULL;
    if (operands == NULL) return NULL;
    if (operands->getExpressionType() == EXPR_EXPRESSIONLIST) {
        opList = static_cast<ExpressionList*>(operands);
    }
    /* need to build all this out */

    switch (fxnIdx) {
        case 0: // date
        case 1: // datetime
        {
            DateFunction* fxn = static_cast<DateFunction*>(functionPtr);
            if (!fxn->setFormatStr && opList != NULL && opList->getExpressions()[1]->getBaseType() == T_STRING) {
                StringExpression* fmtArg = static_cast<StringExpression*>(opList->getExpressions()[1]);
                fxn->setFormat(fmtArg->getValue());
            }
            Expression* argOperand = operands;
            if (opList != NULL) argOperand = opList->getExpressions()[0];
            if (argOperand != NULL && argOperand->getBaseType() == T_INTEGER) {
                NumericalExpression* timeExpr = static_cast<NumericalExpression*>(argOperand);
                retExpression = new StringExpression(fxn->getValue(timeExpr->getIntegerValue()));
            }
            break;
        }
        case 2: // memory
        {
            MemoryFunction* fxn = static_cast<MemoryFunction*>(functionPtr);
            if (!fxn->setFormatStr && opList != NULL && opList->getExpressions()[1]->getBaseType() == T_STRING) {
                StringExpression* fmtArg = static_cast<StringExpression*>(opList->getExpressions()[1]);
                fxn->setFormat(fmtArg->getValue());
            }
            Expression* argOperand = operands;
            if (opList != NULL) argOperand = opList->getExpressions()[0];
            if (argOperand != NULL && argOperand->getBaseType() == T_DOUBLE) {
                NumericalExpression* memExpr = static_cast<NumericalExpression*>(argOperand);
                retExpression = new StringExpression(fxn->getValue(memExpr->getDoubleValue()));
            }
            break;
        }
        case 3: // min
        {
            // need to eventually deal with the output case
            MinFunction* fxn = static_cast<MinFunction*>(functionPtr);
            /* for calculation case, need a list of Numbers */
            if (opList != NULL) {
                bool allNumbers = true;
                int vSize = opList->getExpressions().size();
                double values[vSize];
                double* vPtr = values;
                int idx = 0;
                for (auto iter: opList->getExpressions()) {
                    NumericalExpression* num = static_cast<NumericalExpression*>(iter);
                    if (iter == NULL) values[idx++] = numeric_limits<double>::max(); // field didn't exist for this record
                    else if (iter->getBaseType() == T_INTEGER) values[idx++] = (double) num->getIntegerValue();
                    else if (iter->getBaseType() == T_DOUBLE) values[idx++] = num->getDoubleValue();
                    else values[idx++] = numeric_limits<double>::max(); // can't interpret this field
                }
                if (allNumbers) {
                    double val = fxn->getValue(&vPtr, vSize);
                    if (val != numeric_limits<double>::max()) {
                        retExpression = new NumericalExpression(fxn->getValue(&vPtr, vSize));
                    }
                }
            }
            break;
        }
        case 4: // max
        {
            // need to eventually deal with the output case
            MaxFunction* fxn = static_cast<MaxFunction*>(functionPtr);
            /* for calculation case, need a list of Numbers */
            if (opList != NULL) {
                bool allNumbers = true;
                int vSize = opList->getExpressions().size();
                double values[vSize];
                double* vPtr = values;
                int idx = 0;
                for (auto iter: opList->getExpressions()) {
                    NumericalExpression* num = static_cast<NumericalExpression*>(iter);
                    if (iter == NULL) values[idx++] = -1 * numeric_limits<double>::max(); // field didn't exist for this record
                    else if (iter->getBaseType() == T_INTEGER) values[idx++] = (double) num->getIntegerValue();
                    else if (iter->getBaseType() == T_DOUBLE) values[idx++] = num->getDoubleValue();
                    else values[idx++] = -1 * numeric_limits<double>::max(); // can't interpret this field
                }
                if (allNumbers) {
                    double val = fxn->getValue(&vPtr, vSize);
                    if (val != numeric_limits<double>::min()) {
                        retExpression = new NumericalExpression(fxn->getValue(&vPtr, vSize));
                    }
                }
            }
            break;
        }
        case 5: // sum
        case 6: // count
        case 7: // mean
            break;
        case 8: // contains
        {
            ContainsFunction* fxn = static_cast<ContainsFunction*>(functionPtr);
            auto exprList = opList->getExpressions();
            /* must be two string arguments - period! */
            if (opList != NULL && exprList.size() == 2 && exprList[0]->getBaseType() == T_STRING && exprList[1]->getBaseType() == T_STRING) {
                StringExpression* tgtString = static_cast<StringExpression*>(exprList[0]);
                StringExpression* searchString = static_cast<StringExpression*>(exprList[1]);

                retExpression = new BoolExpression(fxn->getValue(
                            tgtString->getValue().c_str(),
                            searchString->getValue().c_str()
                ));
            } else {
                ExpressionException e(fxnName.c_str(), expressionType, "Invalid arguments for contains.  Takes two STRING type arguments.");
                throw &e;
            }

            break;
        }
    }
    delete operands;
    return retExpression;
}
ostream& FxnExpression::output(ostream& out, DataSource *source, Data** data) {
    out << "UNDEF";
    return out;
}

MonExpression::MonExpression(ExpressionType expressionType, Expression* t_expr):
    expr(t_expr)
{
    this->expressionType = expressionType;
    this->expressionTypeIdentifier = idMap[expressionType];
    this->baseType = T_UNDEF;
    if (expressionType == EXPR_NOT) {
        this->baseType = T_BOOL;
    }
    if (expressionType == EXPR_GROUP) {
        this->baseType = expr->getBaseType();
    }
}
MonExpression::~MonExpression() {
    delete expr;
}
void MonExpression::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    cout << expressionTypeIdentifier << endl;
    expr->printExpressionTree(level+1);
}
bool MonExpression::hasType(ExpressionType type) const {
    return expressionType == type || expr->hasType(type);
}
Expression* MonExpression::evaluateExpression(DataSource *source, Data** data) {
    Expression* eval = expr->evaluateExpression(source, data);

    if (this->expressionType == EXPR_GROUP) {
        return eval;
    }

    if (this->expressionType == EXPR_NOT && eval->getBaseType() == T_BOOL) {
        BoolExpression* b_eval = static_cast<BoolExpression*>(eval);
        b_eval->value = !(b_eval->value);
        return b_eval;
    }

    ExpressionException e("MonExpression", expressionType, "No valid expression or expression type for MonExpression->evaluateExpression to evaluate.");
    throw &e;
}
ostream& MonExpression::output(ostream& out, DataSource *source, Data** data) {
    out << "UNDEF";
    return out;
}

BinExpression::BinExpression(ExpressionType expressionType, Expression* t_left, Expression* t_right):
    left(t_left), right(t_right)
{
    this->expressionType = expressionType;
    this->expressionTypeIdentifier = idMap[expressionType];
    this->baseType = T_UNDEF;

    if (expressionType == EXPR_AND || expressionType == EXPR_OR || expressionType == EXPR_EQUAL || expressionType == EXPR_NEQUAL || expressionType == EXPR_LESS || expressionType == EXPR_GREATER || expressionType == EXPR_LESSEQUAL || expressionType == EXPR_GREATEREQUAL || expressionType == EXPR_REGEXMATCH) {
        this->baseType = T_BOOL;
    }
}

BinExpression::~BinExpression() {
    delete left;
    delete right;
}
void BinExpression::printExpressionTree(int level) {
    for (int i = 0; i < level; i++) cout << "  ";
    cout << expressionTypeIdentifier << endl;
    left->printExpressionTree(level+1);
    right->printExpressionTree(level+1);
}
bool BinExpression::hasType(ExpressionType type) const {
    return expressionType == type || left->hasType(type) || right->hasType(type);
}
Expression* BinExpression::evaluateExpression(DataSource *source, Data** data) {
    //cout << "DEBUG: Eval BinExpression " << this->expressionTypeIdentifier << endl;
    Expression* eval_left = left->evaluateExpression(source, data);
    Expression* eval_right = right->evaluateExpression(source, data);

    if (eval_left == NULL || eval_right == NULL) return NULL;

    switch (expressionType) {
        case EXPR_PLUS:
        case EXPR_MINUS:
        case EXPR_TIMES:
        case EXPR_DIVIDE:
        case EXPR_MODULUS:
        case EXPR_EXPONENT:
            return this->mathOperation(eval_left, eval_right);
            break;
        case EXPR_EQUAL:
        case EXPR_NEQUAL:
        case EXPR_LESS:
        case EXPR_GREATER:
        case EXPR_LESSEQUAL:
        case EXPR_GREATEREQUAL:
            return this->compareOperation(eval_left, eval_right);
            break;
        case EXPR_AND:
        case EXPR_OR:
            return this->booleanOperation(eval_left, eval_right);
            break;
        case EXPR_ASSIGN:
            return this->assignOperation(eval_left, eval_right, data);
            break;
        case EXPR_REGEXMATCH:
            return this->regexMatchOperation(eval_left, eval_right);
            break;
    }
    return NULL;
}

NumericalExpression* BinExpression::mathOperation(Expression* eval_left, Expression* eval_right) {
    if (eval_left == NULL || eval_right == NULL) return NULL;

    if ((eval_left->getBaseType() == T_INTEGER || eval_left->getBaseType() == T_DOUBLE) &&
        (eval_right->getBaseType() == T_INTEGER || eval_right->getBaseType() == T_DOUBLE)) {

        NumericalExpression* neval_left = static_cast<NumericalExpression*>(eval_left);
        NumericalExpression* neval_right = static_cast<NumericalExpression*>(eval_right);

        if (expressionType == EXPR_PLUS) return neval_left->sum(neval_right);
        if (expressionType == EXPR_MINUS) return neval_left->subtract(neval_right);
        if (expressionType == EXPR_TIMES) return neval_left->multiply(neval_right);
        if (expressionType == EXPR_DIVIDE) return neval_left->divide(neval_right);
        if (expressionType == EXPR_MODULUS) return neval_left->modulus(neval_right);
        if (expressionType == EXPR_EXPONENT) return neval_left->exponentiate(neval_right);
    }
    return NULL;
}

BoolExpression* BinExpression::compareOperation(Expression* eval_left, Expression* eval_right) {
    /* need to check types of each expression;
       can compare float/int to float/int
       can compare string to string
       can compare bool to bool
       All other combinations fail
    */
    if (eval_left == NULL || eval_right == NULL) return NULL;

    if ((eval_left->getBaseType() == T_INTEGER || eval_left->getBaseType() == T_DOUBLE) &&
        (eval_right->getBaseType() == T_INTEGER || eval_right->getBaseType() == T_DOUBLE)) {

        NumericalExpression* neval_left = static_cast<NumericalExpression*>(eval_left);
        NumericalExpression* neval_right = static_cast<NumericalExpression*>(eval_right);

        BoolExpression* ret = neval_left->compare(expressionType, neval_right);
        delete neval_left;
        return ret;
    }
    if (eval_left->getBaseType() == T_STRING && eval_right->getBaseType() == T_STRING) {
        StringExpression* s_eval_left = static_cast<StringExpression*>(eval_left);
        StringExpression* s_eval_right = static_cast<StringExpression*>(eval_right);

        BoolExpression* ret = s_eval_left->compare(expressionType, s_eval_right);
        delete s_eval_left;
        return ret;
    }
    if (eval_left->getBaseType() == T_BOOL && eval_right->getBaseType() == T_BOOL) {
        BoolExpression* b_eval_left = static_cast<BoolExpression*>(eval_left);
        BoolExpression* b_eval_right = static_cast<BoolExpression*>(eval_right);

        return b_eval_left->compare(expressionType, b_eval_right);
    }
    return NULL;
}

BoolExpression* BinExpression::booleanOperation(Expression* eval_left, Expression* eval_right) {
    if (eval_left->getBaseType() == T_BOOL && eval_right->getBaseType() == T_BOOL) {
        BoolExpression* b_eval_left = static_cast<BoolExpression*>(eval_left);
        BoolExpression* b_eval_right = static_cast<BoolExpression*>(eval_right);

        return b_eval_left->compare(expressionType, b_eval_right);
    }
    return NULL;
}

BoolExpression* BinExpression::regexMatchOperation(Expression* eval_left, Expression* eval_right) {
    /* eval_left has to evaluate to a string
       eval_right has to be a string -- is the regex
       return boolean of whether or not it matches
    */
    if (
            eval_left == NULL
            || eval_right == NULL
            || eval_left->getBaseType() != T_STRING
            || eval_right->getBaseType() != T_STRING
    ) {
        return NULL;
    }

    StringExpression* s_eval_left = static_cast<StringExpression*>(eval_left);
    StringExpression* s_eval_right = static_cast<StringExpression*>(eval_right);
    BoolExpression *ret = NULL;

    boost::regex reg(s_eval_right->getValue());
    const string &subject = s_eval_left->getValue();
    boost::smatch match_obj;
    if (boost::regex_match(subject, match_obj, reg)) {
        ret = new BoolExpression(true);
    } else {
        ret = new BoolExpression(false);
    }
    delete s_eval_left;
    delete s_eval_right;
    return ret;
}

Expression* BinExpression::assignOperation(Expression* eval_left, Expression* eval_right, Data** data) {
    /* eval_left has to be a NameExpression
       eval_right has to be a primitive of some type: String, Int, Float, or Bool
       need to store the right value into data[eval_left->varIdx], then
       return right value
    */
    if (eval_left->getExpressionType() != EXPR_NAME) return NULL;
    NameExpression* varExpr = static_cast<NameExpression*>(eval_left);
    //if (data[varExpr->varIdx] == NULL) data[varExpr->varIdx] = new Data();

    if (eval_right->getBaseType() == T_INTEGER || eval_right->getBaseType() == T_DOUBLE) {
        NumericalExpression* intExpr = static_cast<NumericalExpression*>(eval_right);
        if (intExpr->getBaseType() == T_INTEGER) {
            (*data)[varExpr->varIdx].setValue(intExpr->i_value);
        } else {
            (*data)[varExpr->varIdx].setValue(intExpr->d_value);
        }
        delete eval_left;
        return eval_right;
    }
    if (eval_right->getBaseType() == T_STRING) {
        StringExpression* strExpr = static_cast<StringExpression*>(eval_right);
        (*data)[varExpr->varIdx].setValue(strExpr->value);
        delete eval_left;
        return eval_right;
    }
    if (eval_right->getBaseType() == T_BOOL) {
        BoolExpression* bExpr = static_cast<BoolExpression*>(eval_right);
        (*data)[varExpr->varIdx].setValue(bExpr->value);
        delete eval_left;
        return eval_right;
    }

    return NULL;
}
ostream& BinExpression::output(ostream& out, DataSource *source, Data** data) {
    out << "UNDEF";
    return out;
}

}
#endif /* __NCLQ_PARSETREE */
