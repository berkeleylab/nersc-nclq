/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
/* BISON Declarations */
%skeleton "lalr1.cc"
%require  "3.0"
%debug 
%defines 
%define api.namespace {nclq}
%define parser_class_name {BisonParser}

%code requires {
    namespace nclq {
        class Scanner;
        class nclqParser;
    }
#include "nclq.hh"
}

%parse-param {Scanner &scanner}
%parse-param {nclqParser *parser}

%code {
#include "nclq.hh"
#include "parser.hh"
#include <iostream>
#include <string>
#include <stack>
#include "nclq.hh"
#include "Scanner.hh"
#undef yylex
#define yylex scanner.yylex
}

%token NAME
%token FLOAT
%token INTEGER
%token DATE
%token DATETIME
%token TIME
%token STRING
%token PLUS
%token MINUS
%token TIMES
%token DIVIDE
%token MODULUS
%token EXPONENT
%token EQUAL
%token NEQUAL
%token REGEXMATCH
%token LESS
%token GREATER
%token LESSEQUAL
%token GREATEREQUAL
%token AND
%token OR
%token NOT
%token LPAREN
%token RPAREN
%token ASSIGN
%token COMMA
%token TRUE
%token FALSE

%left COMMA
%left ASSIGN
%left OR
%left AND
%left EQUAL NEQUAL REGEXMATCH
%left LESS GREATER LESSEQUAL GREATEREQUAL
%left PLUS MINUS
%left TIMES DIVIDE MODULUS
%right EXPONENT
%right UMINUS NOT

%union {
    Expression* exp;
    ExpressionList* expList;
	intType   i_value;
	double    d_value;
    char *s_value;
}

/* Lets inform Bison about the type of each terminal and non-terminal */
%type <exp>   expression
%type <expList> exprList
%type <i_value> INTEGER
%type <i_value> DATETIME
%type <i_value> DATE
%type <i_value> TIME
%type <d_value> FLOAT
%type <s_value> STRING
%type <s_value> NAME


%%

expression :  MINUS expression %prec UMINUS { $$ = new BinExpression(EXPR_TIMES, new NumericalExpression((intType) -1), $2); parser->parseStack.pop(); parser->parseStack.push($$); }
            | NOT expression                { $$ = new MonExpression(EXPR_NOT, $2); parser->parseStack.pop(); parser->parseStack.push($$); }
            | LPAREN expression RPAREN      { $$ = new MonExpression(EXPR_GROUP, $2); parser->parseStack.pop(); parser->parseStack.push($$); }
            | STRING                        { $$ = new StringExpression($1, true); free($1); parser->parseStack.push($$); }
			| NAME							{ $$ = new NameExpression($1, parser->symbols); free($1); parser->parseStack.push($$); }
            | INTEGER                       { $$ = new NumericalExpression($1); parser->parseStack.push($$); }
            | DATETIME                      { $$ = new NumericalExpression($1); parser->parseStack.push($$); }
            | DATE                          { $$ = new NumericalExpression($1); parser->parseStack.push($$); }
            | TIME                          { $$ = new NumericalExpression($1); parser->parseStack.push($$); }
			| FLOAT                         { $$ = new NumericalExpression($1); parser->parseStack.push($$); }
            | TRUE                          { $$ = new BoolExpression(true); parser->parseStack.push($$); }
            | FALSE                         { $$ = new BoolExpression(false); parser->parseStack.push($$); }
			| expression PLUS expression    { $$ = new BinExpression(EXPR_PLUS, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression MINUS expression   { $$ = new BinExpression(EXPR_MINUS, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression TIMES expression   { $$ = new BinExpression(EXPR_TIMES, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression DIVIDE expression  { $$ = new BinExpression(EXPR_DIVIDE, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression MODULUS expression { $$ = new BinExpression(EXPR_MODULUS, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression EXPONENT expression{ $$ = new BinExpression(EXPR_EXPONENT, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression EQUAL expression   { $$ = new BinExpression(EXPR_EQUAL, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression NEQUAL expression  { $$ = new BinExpression(EXPR_NEQUAL, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression REGEXMATCH expression  { $$ = new BinExpression(EXPR_REGEXMATCH, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression LESS expression    { $$ = new BinExpression(EXPR_LESS, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression GREATER expression { $$ = new BinExpression(EXPR_GREATER, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression LESSEQUAL expression { $$ = new BinExpression(EXPR_LESSEQUAL, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression GREATEREQUAL expression { $$ = new BinExpression(EXPR_GREATEREQUAL, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression AND expression     { $$ = new BinExpression(EXPR_AND, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression OR expression      { $$ = new BinExpression(EXPR_OR, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| expression ASSIGN expression  { $$ = new BinExpression(EXPR_ASSIGN, $1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
			| NAME LPAREN expression RPAREN { $$ = new FxnExpression($1, $3, parser->symbols); free($1); parser->parseStack.pop(); parser->parseStack.push($$); }
			| NAME LPAREN exprList RPAREN   { $$ = new FxnExpression($1, $3, parser->symbols); free($1); parser->parseStack.pop(); parser->parseStack.push($$); }
		    | exprList                      { $$ = $1; }

exprList : expression COMMA expression      { $$ = new ExpressionList($1, $3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }
         | exprList COMMA expression        { $$ = static_cast<ExpressionList*>($1); $1->addExpression($3); parser->parseStack.pop(); parser->parseStack.pop(); parser->parseStack.push($$); }

%%

void nclq::BisonParser::error(const std::string &err_message) {
    std::cerr << "ERROR: " << err_message << std::endl;
}
