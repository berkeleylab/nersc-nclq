/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#include "nclq.hh"
#include "Scanner.hh"
#include <iostream>
#include <algorithm>
#include <sstream>
#include <math.h>

using namespace std;

namespace nclq {

SymbolTable::SymbolTable(const vector<VarDescriptor>& _vars)
{
    int idx = 0;
    for (auto it: _vars) {
        variables.push_back(it);
        variableMap[it.name] = idx++;
    }
    allowModification = false;
}

ostream& operator<<(ostream &out, Data& data) {
    if (data.type == T_INTEGER) {
        out << data.data.i;
    } else if (data.type == T_DOUBLE) {
        out << data.data.d;
    } else if (data.type == T_STRING) {
        out << data.data.s;
    } else if (data.type == T_BOOL) {
        out << data.data.b;
    } else {
        out << "UNKNOWN TYPE";
    }
}

void SymbolTable::printSymbolTable() {
    cout << "=======================================" << endl;
    vector<string> keys;
    unordered_map<size_t, vector<string> > inverse_map;
    for (auto it: variableMap) {
        keys.push_back(it.first);
        size_t inv_key = it.second;
        auto it2 = inverse_map.find(inv_key);
        if (it2 == inverse_map.end()) {
            vector<string> value = {it.first};
            inverse_map.emplace(make_pair(inv_key,  value));
        } else {
            inverse_map[inv_key].push_back(it.first);
        }
    }
    sort(keys.begin(), keys.end());
    for (auto it: inverse_map) {
        vector<string> &a = it.second;
        sort(a.begin(), a.end());
    }
    for (auto key: keys) {
        size_t idx = variableMap[key];
        vector<string> &raw_aliases = inverse_map[idx];
        vector<string> aliases(raw_aliases.size());
        auto it = copy_if(raw_aliases.begin(), raw_aliases.end(), aliases.begin(), [&key](const string& s){return s != key;});
        aliases.resize(distance(aliases.begin(), it));
        sort(aliases.begin(), aliases.end());
        cout << key << "(";
        switch (variables[idx].type) {
            case T_INTEGER: cout << "integer"; break;
            case T_DOUBLE: cout << "real number"; break;
            case T_STRING: cout << "string"; break;
            case T_BOOL: cout << "bool"; break;
            case T_MEMORY: cout << "memory"; break;
            case T_USER: cout << "user"; break;
            case T_GROUP: cout << "group"; break;
            case T_UINT: cout << "unsigned integer"; break;
            case T_TIME: cout << "time"; break;
            case T_UNDEF: cout << "undefined"; break;
        }
        cout << ");";
        if (strlen(variables[idx].summary) > 0) {
            cout << " " << variables[idx].summary;
        }
        if (aliases.size() > 0) {
            cout << "; Aliases: ";
            for (size_t idx = 0; idx < aliases.size(); idx++) {
                if (idx != 0) cout << ", ";
                cout << aliases[idx];
            }
        }
        cout << endl;
    }
    cout << "=======================================" << endl;
    cout << endl;
}

nclqParser::nclqParser(SymbolTable *_symbols) {
    symbols = _symbols;
    scanner = NULL;
    verbose = false;

    scanner = new Scanner(NULL);
    parser = new BisonParser(*scanner, this);
}

nclqParser::~nclqParser() {
    if (scanner != NULL) {
        delete scanner;
        scanner = NULL;
    }
    if (parser != NULL) {
        delete parser;
        parser = NULL;
    }
}

void nclqParser::parseExpression(const vector<string>& in_expressions,
        vector<Expression *>& out_expressions,
        void (*checkFxn)(const string &, const Expression *))
{
    vector<Expression *> cache_expressions;

    for (auto in_it: in_expressions) {
        istringstream iss(in_it);
        scanner->yyrestart(&iss);
        int parseStatus = -1;

        try {
            parseStatus = parser->parse();
        } catch (ExpressionException &e) {
            for (auto it: cache_expressions) delete it;
            throw e;
        }

        if (parseStatus == 0 && parseStack.size() > 0) {
            Expression *root = parseStack.top();
            while (parseStack.size() > 0) parseStack.pop();
            if (root->expressionType == EXPR_EXPRESSIONLIST) {
                ExpressionList *expList = static_cast<ExpressionList*>(root);
                if (verbose) {
                    for (auto it: expList->expressions) {
                        it->printExpressionTree(0);
                    }
                }
                if (checkFxn != NULL) {
                    for (auto it: expList->expressions) {
                        try {
                            checkFxn(in_it, it);
                        } catch (invalid_argument &e) {
                            if (verbose) cout << "Caught invalid_argument exception: " << e.what() << "; cleaning up and re-throwing" << endl;
                            delete expList;
                            for (auto it: cache_expressions) delete it;
                            throw e;
                        }
                    }
                }
                cache_expressions.insert(cache_expressions.end(), expList->expressions.begin(), expList->expressions.end());
                expList->expressions.clear();
                delete expList;
            } else {
                if (verbose) {
                    root->printExpressionTree(0);
                }
                if (checkFxn != NULL) {
                    try {
                        checkFxn(in_it, root);
                    } catch (invalid_argument &e) {
                        if (verbose) cout << "Caught invalid_argument exception: " << e.what() << "; cleaning up and re-throwing" << endl;
                        delete root;
                        for (auto it: cache_expressions) delete it;
                        throw e;
                    }
                }
                cache_expressions.push_back(root);
            }
        }
    }
    out_expressions.insert(out_expressions.end(), cache_expressions.begin(), cache_expressions.end());
}

namespace util {
const double convertMemory(const char *str) {
    if (str == NULL) {
        invalid_argument e("memory conversion must not be zero length");
        throw &e;
    }
    char *suffixPtr = NULL;
    double prefix = strtod(str, &suffixPtr);
    char suffix = 0;
    if (suffixPtr != NULL) {
        suffix = toupper(*suffixPtr);
    }

    int exponent = 0;
    switch (suffix) {
        case 'K': exponent = 1; break;
        case 'M': exponent = 2; break;
        case 'G': exponent = 3; break;
        case 'T': exponent = 4; break;
        case 'P': exponent = 5; break;
        case 'E': exponent = 6; break;
    }
    if (suffix != 0 && (suffix < '0' || suffix > '9') && exponent == 0) {
        invalid_argument e("invalid format for memory description");
        throw &e;
    }
    return prefix * pow(1024.0, exponent);
}

const intType convertTime(const char *str) {
    int vals[3] = {0,0,0};
    int mult[3][3] = { {1, 1, 1}, {60, 1, 1}, {3600, 60, 1} };
    const char *startPtr = str;
    const char *ptr = NULL;
    int count = 0;
    for (int idx = 0; idx < 3; idx++) {
        vals[idx] = atoi(startPtr);
        ptr = strchr(startPtr, ':');
        if (ptr == NULL) break;
        startPtr = ptr + 1;
        count++;
    }
    return vals[0]*mult[count][0] + vals[1]*mult[count][1] + vals[2]*mult[count][2];
}
} /* end namespace util */
} /* end namespace nclq */
