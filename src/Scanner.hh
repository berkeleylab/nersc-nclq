/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#ifndef __NCLQ_SCANNER_HPP
#define __NCLQ_SCANNER_HPP

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "parser.hh"

namespace nclq {

class Scanner : public yyFlexLexer {
    public:
    Scanner(std::istream *in): yyFlexLexer(in), yylval(NULL) {}
    int yylex(BisonParser::semantic_type *lval) {
        yylval = lval;
        return yylex();
    }

    private:
    BisonParser::semantic_type *yylval;
    int yylex();
};

}

#endif
