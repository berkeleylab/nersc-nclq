/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
#ifndef __NCLQ_HH
#define __NCLQ_HH

#include <string>
#include <vector>
#include <stack>
#include <stdexcept>
#include <unordered_map>
#include <string.h>

typedef ssize_t intType;

using namespace std;

namespace nclq {

namespace util {
    const double convertMemory(const char *);
    const intType convertTime(const char *);
}

enum SymbolType {
    T_INTEGER = 0,
    T_DOUBLE,
    T_STRING,
    T_BOOL,
    T_MEMORY,
    T_USER,
    T_GROUP,
    T_UINT,
    T_TIME,
    T_UNDEF
};

enum ExpressionType {
    EXPR_UNKNOWN,
    EXPR_NAME,
    EXPR_FLOAT,
    EXPR_INTEGER,
    EXPR_STRING,
    EXPR_PLUS,
    EXPR_MINUS,
    EXPR_TIMES,
    EXPR_DIVIDE,
    EXPR_BOOL,
    EXPR_MODULUS,
    EXPR_EXPONENT,
    EXPR_EQUAL,
    EXPR_NEQUAL,
    EXPR_LESS,
    EXPR_GREATER,
    EXPR_LESSEQUAL,
    EXPR_GREATEREQUAL,
    EXPR_AND,
    EXPR_OR,
    EXPR_NOT,
    EXPR_GROUP,
    EXPR_ASSIGN,
    EXPR_VARIABLE,
    EXPR_FUNCTION,
    EXPR_EXPRESSIONLIST,
    EXPR_REGEXMATCH,
};

struct DataRep {
    double d;
    intType i;
    string s;
    char c;
    bool b;

    DataRep() {
        d = 0;
        i = 0;
        c = 0;
        b = false;
    }
    ~DataRep() {
    }
};

class Data {
    SymbolType type;
    DataRep data;
    bool set;

    public:
    Data() {
        set = false;
    }

    const double getDoubleValue() {
        return data.d;
    }
    const intType getIntValue() {
        return data.i;
    }
    const string& getStringValue() {
        return data.s;
    }
    const bool getBoolValue() {
        return data.b;
    }

    SymbolType getType() {
        return type;
    }

    void setValue(intType value, SymbolType t_type=T_INTEGER) {
        data.i = value;
        set = true;
        type = t_type;
    }
    void setValue(double value, SymbolType t_type=T_DOUBLE) {
        data.d = value;
        set = true;
        type = t_type;
    }

    void setValue(const char* value, SymbolType t_type=T_STRING) {
        data.s = value;
        set = true;
        type = t_type;
    }
    void setValue(const string& value, SymbolType t_type=T_STRING) {
        data.s = value;
        set = true;
        type = t_type;
    }
    void setValue(bool value, SymbolType t_type=T_BOOL) {
        data.b = value;
        set = true;
        type = t_type;
    }

    inline bool isSet() {
        return set;
    }
    friend ostream& operator<<(ostream &out, Data& data);
};

class DataSource {
    public:
    virtual bool getNext() = 0;
    virtual bool setValue(int idx, Data *data, SymbolType outputType) = 0;
    virtual int size() = 0;
    virtual void outputValue(ostream& out, int idx) = 0;
};

ostream& operator<<(ostream &out, Data& data);
class Expression;
class ExpressionList;
class Function;
class Scanner;
class BisonParser;

static int const varDescriptorNameLen = 30;
static int const varDescriptorSummaryLen = 64;
class VarDescriptor {
    public:
    char name[varDescriptorNameLen];
    char summary[varDescriptorSummaryLen];
    SymbolType type;
    SymbolType outputType;

    VarDescriptor(const char *_name, const char *_summary,
            const SymbolType _type, const SymbolType _outputType
    ) {
        strncpy(name, _name, varDescriptorNameLen);
        strncpy(summary, _summary, varDescriptorSummaryLen);
        type = _type;
        outputType = _outputType;
    }
    VarDescriptor(const char *_name,
            const SymbolType _type, const SymbolType _outputType
    ) {
        strncpy(name, _name, varDescriptorNameLen);
        summary[0] = 0;
        type = _type;
        outputType = _outputType;
    }
};

class SymbolTable {
    bool allowModification;
    vector<VarDescriptor> variables;
    unordered_map<string,size_t> variableMap;

    public:
    SymbolTable(const vector<VarDescriptor>&);
    void printSymbolTable();
    inline const bool modifiable() const { return allowModification; }
    inline void setModifiable(bool mod) { allowModification = mod; }
    ssize_t addSymbol(string& name, VarDescriptor& var) {
        if (allowModification) {
            size_t varIdx = variables.size();
            variables.push_back(var);
            variableMap[name] = varIdx;
            return varIdx;
        }
        return -1;
    }
    bool mapSymbol(const string& key, size_t index) {
        if (allowModification && index >= 0 && index < variables.size()) {
            variableMap[key] = index;
            return true;
        }
        return false;
    }
    const ssize_t find(const string &name) const {
        auto it = variableMap.find(name);
        if (it != variableMap.end()) {
            return it->second;
        }
        return -1;
    }
    const VarDescriptor& operator[](size_t idx) const {
        return variables[idx];
    }
    inline const size_t size() const {
        return variables.size();
    }
    inline const unordered_map<string,size_t>& getVariableMap() const {
        return variableMap;
    }
};

class nclqParser {
    friend class Scanner;
    friend class BisonParser;
    bool verbose;
    SymbolTable *symbols;
    stack<Expression*> parseStack;
    Scanner *scanner;
    BisonParser *parser;

    public:
    nclqParser(SymbolTable *);
    ~nclqParser();

    inline void setVerbose(bool _val) { verbose = _val; }

    void parseExpression(const vector<string>& in_expressions,
        vector<Expression *>& out_expressions,
        void (*checkFxn)(const string &, const Expression *)
    );
};

class ExpressionException : public runtime_error {
    private:
	const char* token;
	ExpressionType type;

    public:
	ExpressionException(const char*, ExpressionType, const char *);
	ExpressionException(const char*, ExpressionType, string&);
	const char* getToken();
};

class Expression {
    friend class nclqParser;
    protected:
	ExpressionType expressionType;
	SymbolType baseType;
	const char* expressionTypeIdentifier;

    public:
	Expression();
	virtual ~Expression();
	virtual void printExpressionTree(int level);
	virtual bool hasType(ExpressionType) const;
	virtual Expression* evaluateExpression(DataSource *, Data**);
	virtual ostream& output(ostream&, DataSource *, Data**);
    const ExpressionType getExpressionType() const;
    const SymbolType getBaseType() const;
};

class BoolExpression : public Expression {
    friend class MonExpression;
    friend class BinExpression;
    friend class NumericalExpression;
    friend class StringExpression;
    protected:
	bool value;

    public:
	BoolExpression(bool t_value);
	virtual void printExpressionTree(int level);
	virtual Expression* evaluateExpression(DataSource *source, Data** data);
	BoolExpression* compare(int type, BoolExpression* b);
	ostream& output(ostream& out, DataSource *source, Data** data);
    const bool getValue() const;
};

class NumericalExpression : public Expression {
    friend class MonExpression;
    friend class BinExpression;
    protected:
	intType i_value;
	double d_value;

    public:
	NumericalExpression* sum(NumericalExpression* ptr);
	NumericalExpression* multiply(NumericalExpression* ptr);
	NumericalExpression* divide(NumericalExpression* ptr);
	NumericalExpression* subtract(NumericalExpression* ptr);
	NumericalExpression* modulus(NumericalExpression* ptr);
	NumericalExpression* exponentiate(NumericalExpression* ptr);
	NumericalExpression(intType t_value);
	NumericalExpression(double t_value);
	void printExpressionTree(int level);
	Expression* evaluateExpression(DataSource *source, Data** data);
	BoolExpression* compare(int type, NumericalExpression* b);
	ostream& output(ostream& out, DataSource *source, Data** data);
    const intType getIntegerValue() const;
    const double getDoubleValue() const;
};

class StringExpression : public Expression {
    friend class MonExpression;
    friend class BinExpression;
    protected:
    string value;
	bool owner;

    public:
	StringExpression(const char* t_value, bool t_alloc=false);
    StringExpression(const string& t_value, bool t_alloc=false);
	~StringExpression();
	void printExpressionTree(int level);
	Expression* evaluateExpression(DataSource *source, Data** data);
	BoolExpression* compare(int type, StringExpression* b);
	ostream& output(ostream& out, DataSource *source, Data** data);
    const string& getValue() const;
};

class NameExpression : public Expression {
    friend class MonExpression;
    friend class BinExpression;
    protected:
	string varName;
	size_t varIdx;
	SymbolType varType;
	SymbolType varOutputType;
	SymbolTable* symbols;

    public:
	NameExpression(const char*, SymbolTable*);
	NameExpression(const string&, int, SymbolType, SymbolTable*);
	void printExpressionTree(int level);
	Expression* evaluateExpression(DataSource *source, Data** data);
	ostream& output(ostream& out, DataSource *source, Data **data);
    inline const size_t getVariableIndex() const { return varIdx; }
};

class ExpressionList : public Expression {
    friend class MonExpression;
    friend class BinExpression;
    friend class nclqParser;
    private:
	void init();

    protected:
	vector<Expression*> expressions;

    public:
	ExpressionList();
	ExpressionList(Expression* expr1, Expression* expr2);
	virtual ~ExpressionList();
	void addExpression(Expression* expr1);
	void printExpressionTree(int level);
	virtual bool hasType(ExpressionType type) const;
	Expression* evaluateExpression(DataSource *source, Data** data);
	ostream& output(ostream& out, DataSource *source, Data** data);
    const vector<Expression*>& getExpressions() const;
};

class FxnExpression : public Expression {
    friend class MonExpression;
    friend class BinExpression;
    protected:
	int fxnIdx;
	Function* functionPtr;
	string fxnName;
	Expression* expr;
	SymbolTable* symbols;

    public:
	FxnExpression(const char* t_name, Expression* t_expr, SymbolTable* _symbols);
	~FxnExpression();
	void printExpressionTree(int level);
	virtual bool hasType(ExpressionType type) const;
	Expression* evaluateExpression(DataSource *source, Data** data);
	ostream& output(ostream& out, DataSource *source, Data** data);
};

class MonExpression : public Expression {
    protected:
	Expression* expr;

    public:
	MonExpression(ExpressionType expressionType, Expression* t_expr);
	~MonExpression();
	void printExpressionTree(int level);
	virtual bool hasType(ExpressionType type) const;
	Expression* evaluateExpression(DataSource *source, Data** data);
	ostream& output(ostream& out, DataSource *source, Data** data);
};

class BinExpression : public Expression {
    protected:
	Expression* left;
	Expression* right;

    public:
	BinExpression(ExpressionType expressionType, Expression* t_left, Expression* t_right);
	~BinExpression();
	void printExpressionTree(int level);
	virtual bool hasType(ExpressionType type) const;
	Expression* evaluateExpression(DataSource *, Data** );
	NumericalExpression* mathOperation(Expression* , Expression* );
	BoolExpression* compareOperation(Expression* , Expression* );
	BoolExpression* booleanOperation(Expression* , Expression* );
    BoolExpression* regexMatchOperation(Expression*, Expression *);
	Expression* assignOperation(Expression* , Expression* , Data** );
	ostream& output(ostream& out, DataSource *source, Data** data);
};

}

#endif // __NCLQ_HH
