/*******************************************************************************
qqacct, Copyright (c) 2014, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of any
required approvals from the U.S. Dept. of Energy).  All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Technology Transfer Department at  TTD@lbl.gov.

The LICENSE file in the root directory of the source code archive describes the
licensing and distribution rights and restrictions on this software.

Author:   Douglas Jacobsen <dmj@nersc.gov>
*******************************************************************************/
%option debug
%option c++
%option noyywrap
%option yyclass="nclq::Scanner"

%{
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "nclq.hh"
#include "parser.hh"
#include "Scanner.hh"
typedef nclq::BisonParser::token token;
%}

D			[0-9]
IDENTIFIER	[a-zA-Z][a-zA-Z0-9_\.]*

%%

"true"      return token::TRUE;
"false"     return token::FALSE;

{D}+	{
				yylval->i_value = atoi(yytext);
				return token::INTEGER;
			}

{D}+"."{D}*?(E{D}+)?	{
				yylval->d_value = atof(yytext);
				return token::FLOAT;
			}

{D}+("."{D}*?(E{D}+)?)?[kKmMgGtTpP] {
				char* typeptr = yytext + strlen(yytext) - 1;
				int exp = 0;
                char expChar = toupper(*typeptr);
                switch (expChar) {
                    case 'K': exp = 1; break;
                    case 'M': exp = 2; break;
                    case 'G': exp = 3; break;
                    case 'T': exp = 4; break;
                    case 'P': exp = 5; break;
                    case 'E': exp = 6; break;
                }
				yylval->d_value = atof(yytext) * pow(1024, exp);
				return token::FLOAT;
			}

\"?{D}{D}{D}{D}-{D}{D}-{D}{D}\"? {
				struct tm theDate;
				bzero(&theDate, sizeof(struct tm));
				theDate.tm_isdst = -1; //auto-detect DST
				char* ptr = yytext;
				if (yytext[0] == '\"') {
					ptr++;
				}
				strptime(ptr, "%Y-%m-%d", &theDate);
				time_t seconds = mktime(&theDate);
				yylval->i_value = seconds;
				return token::DATE;
			}

\"?{D}{D}:{D}{D}(:{D}{D})?\"? {
				int blocks[3] = {0,0,0};
				int seconds = 0;
				int block_idx = 0;
				char* s_ptr = yytext;
				if ( *s_ptr == '\"') s_ptr++;
				for (char* ptr = s_ptr; (ptr-yytext)<=yyleng; ptr++) {
					if (*ptr == ':' || *ptr == 0 || *ptr == '\"' || ptr-yytext == yyleng) {
						*ptr = 0;
						blocks[block_idx++] = atoi(s_ptr);
						s_ptr = ptr+1;
						ptr = s_ptr;
					}
				}
				seconds = blocks[0]*3600 + blocks[1]*60 + blocks[2];
				yylval->i_value = seconds;
				return token::TIME;
			}

\"?{D}{D}{D}{D}-{D}{D}-{D}{D}" "{D}{D}:{D}{D}:{D}{D}\"? {
				struct tm theDate;
				bzero(&theDate, sizeof(struct tm));
				theDate.tm_isdst = -1; //auto-detect DST
				char* ptr = yytext;
				if (yytext[0] == '\"') {
					ptr++;
				}
				strptime(ptr, "%Y-%m-%d %H:%M:%S", &theDate);
				time_t seconds = mktime(&theDate);
				yylval->i_value = seconds;
				return token::DATETIME;
			}


\"[^"]*?\"		{
				yytext[strlen(yytext)-1] = 0;
				yylval->s_value = strdup(yytext+1);
				return token::STRING;
			}

{IDENTIFIER}	{
				yylval->s_value = strdup(yytext);
				return token::NAME;
			}

"+"		return token::PLUS;
"-"		return token::MINUS;
"**"	return token::EXPONENT;
"*"		return token::TIMES;
"/"		return token::DIVIDE;
"%"		return token::MODULUS;
"^"		return token::EXPONENT;
"=="	return token::EQUAL;
"!="	return token::NEQUAL;
"=~"    return token::REGEXMATCH;
"<="	return token::LESSEQUAL;
">="	return token::GREATEREQUAL;
"<"		return token::LESS;
">"		return token::GREATER;
"="		return token::ASSIGN;
"&&"	return token::AND;
"||"	return token::OR;
"!"		return token::NOT;
"("		return token::LPAREN;
")"		return token::RPAREN;
","		return token::COMMA;

[ \t\n]+		/* skip whitespace */
\0      yyterminate();

%%

